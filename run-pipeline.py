#! /usr/bin/python3
import os
import argparse
import yaml
import re
import subprocess
from pwd import getpwnam


DEFAULT_YAML = 'debian/gitlab-ci.yml'
WORKING_DIR = '/root/pipeline'
ARTIFACTS_TMP_DIR = '/tmp/artifacts'

GITLAB_VARIABLES = {}


def load_yaml(path):
    with open(path, 'r') as stream:
        return yaml.load(stream)

def get_valid(yaml, job, key):
    try:
        return yaml[job][key]
    except KeyError:
        try:
            return yaml[key]
        except KeyError:
            return None

def string_eval(string, variables):
    for var in variables.keys():
        string = string.replace('${{{}}}'.format(var), str(variables[var]))
        string = string.replace('${}'.format(var), str(variables[var]))
    return string


def list_eval(list, variables):
    return [replace_variables(string, variables) for string in list]


def dict_eval(dict, variables):
    for key in dict.keys():
        dict[key] = replace_variables(dict[key], variables)
    return dict


def replace_variables(o, variables):
    """Replaces variables inside of the yml"""
    if type(o) is str:
        result = string_eval(o, variables)
    elif type(o) is list:
        result = list_eval(o, variables)
    elif type(o) is dict:
        result = dict_eval(o, variables)
    elif o is None:
        return o
    else:
        raise Exception("can't parse %s" % type(o))

    undefined = re.findall(r'\${?\w+}?', str(result))
    if undefined:
        raise Exception("Some variables are undefined: %s" % undefined)

    return result


def parse_pipeline(args):
    pipeline = load_yaml(args.yaml_file)

    job = args.job
    GITLAB_VARIABLES['CI_BUILD_NAME'] = job
    try:
        job_details = pipeline[job]
    except KeyError:
        raise Exception('No job named %s', job)

    image = args.image or get_valid(pipeline, job, 'image')
    if not image:
        raise Exception('No image setted')

    before_script = get_valid(pipeline, job, 'before_script') or []
    after_script = get_valid(pipeline, job, 'after_script') or []
    script = get_valid(pipeline, job, 'script') or []

    full_script = before_script + script + after_script

    artifacts = get_valid(pipeline, job, 'artifacts')

    variables = get_valid(pipeline, job, 'variables') or {}
    variables.update(GITLAB_VARIABLES)
    variables.update(os.environ)

    full_script = replace_variables(full_script, variables)
    image = replace_variables(image, variables)
    artifacts = replace_variables(artifacts, variables)

    return full_script, image, artifacts


def run_in_docker(container_id, command):
    docker_exec = ['docker', 'exec', container_id, 'sh', '-c']
    return docker_exec + [command]


def run(args):
    script, image, artifacts = parse_pipeline(args)

    try:
        docker_options = [
            '--detach=true', '-w=/root/pipeline',
            '--volume', '%s:%s' % (args.artifacts_path, ARTIFACTS_TMP_DIR),
        ]
        if args.privileged:
            docker_options.append('--privileged')

        container_id = subprocess.check_output(
            ['docker', 'run'] + docker_options + [image, 'sleep', 'infinity']
        ).decode('UTF-8').strip()

        if args.pwd:
            subprocess.check_call(['docker', 'cp', '{}/.'.format(args.pwd), '{}:{}'.format(container_id, WORKING_DIR)])

        if args.proxy:
	        subprocess.check_call(run_in_docker(container_id, "echo 'Acquire::http {{ Proxy \"{proxy}\"; }};' > /etc/apt/apt.conf.d/01proxy".format(proxy=args.proxy)))

        for line in script:
            subprocess.check_call(run_in_docker(container_id, line))

        for path in artifacts['paths']:
            subprocess.check_call(run_in_docker(container_id, 'mv {} {}'.format(path, ARTIFACTS_TMP_DIR)))

        if args.user:
            user = getpwnam(args.user)
            subprocess.check_call(run_in_docker(container_id, 'chown -R {}:{} {}'.format(user.pw_uid, user.pw_gid, ARTIFACTS_TMP_DIR)))

    except Exception as e:
        raise e

    finally:
        subprocess.check_call(['docker', 'stop', container_id])
        subprocess.check_call(['docker', 'rm', '-f', container_id])


if __name__ == '__main__':
    default_proxy = ''
    try:
        p = subprocess.check_output(
            'eval `apt-config shell p Acquire::http::Proxy`; echo $p',
            shell=True, universal_newlines=True).strip()
        if p:
            docker_ip = subprocess.check_output(
                '''ip -4 a show dev "docker0" | awk '/ inet / {sub(/\/.*$/, "", $2); print $2}' ''',
                shell=True, universal_newlines=True).strip()
            default_proxy = re.sub('localhost|127\.0\.0\.[0-9]*', docker_ip, p)
    except subprocess.CalledProcessError:
        pass

    parser = argparse.ArgumentParser(
        '''
        Run gitlab-ci pipelines in locally
        '''
    )
    parser.add_argument('-y', '--yaml-file', help="Where the pipeline is defined. (Default: %s)" % DEFAULT_YAML, default=DEFAULT_YAML)
    parser.add_argument('-i', '--image', help="Image to use to override job's image, or if the job has not an image defined.")
    parser.add_argument('-a', '--artifacts-path', help="Where to leave the job's artifact")
    parser.add_argument('-u', '--user', help="Change artifacts ownership to this user")
    parser.add_argument('--pwd', help="Path to COPY into the container's pwd. It won't be modified. Usually, this is the repo path.")
    parser.add_argument('--privileged', action="store_true",  help="Container is run as --privileged")
    parser.add_argument('job', help="Jobs' name")
    parser.add_argument('-p', '--proxy', metavar='URL', default=default_proxy,
                        help='Use a proxy for apt (default: %s)' % (default_proxy or 'none'))
    args = parser.parse_args()

    run(args)
